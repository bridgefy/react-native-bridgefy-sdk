# 🚧  **DEPRECATED** 🚧 no longer actively maintained 

---

#### How to get the official react-native-bridgefy version?

1. Go to https://github.com/bridgefy/react-native-bridgefy
1. Go to the root directory of the project and run the following command:
`npm install --save react-native-bridgefy` or `yarn add react-native-bridgefy`

#### Do we ever un-deprecate this project??

We try to make sure nobody is using a project anymore before we archive it. However, if you depend on the project remaining as an accessible Bitbucket repository, then send a message to contact@bridgefy.me.

